package com.hendisantika.springredissentinel.controller;

import com.hendisantika.springredissentinel.entity.Hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-redis-sentinel
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/07/18
 * Time: 07.42
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class HelloController {

    @Autowired
    private CacheManager cacheManager;

    @PostMapping("/hello")
    public Hello save() {
        Hello hello = new Hello(1L, "naruto", "12300199", "naruto@test.com");
        Cache cache = cacheManager.getCache("cache.hello");
        cache.put("hello", hello);
        return hello;
    }

    @GetMapping("/hello")
    public Hello getHello() {
        Cache cache = cacheManager.getCache("cache.hello");
        return cache.get("hello", Hello.class);
    }
}